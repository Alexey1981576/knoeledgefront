import { DOCUMENT } from "@angular/common";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Component, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Question } from "src/app/Interfaces/interfaces";
import { HttpConnectionService } from "src/app/services/auth.service";
import { StorageService } from "src/app/services/storage.service";

@Component({
    selector:'delete-question',
    templateUrl:'delete.component.html',
    styleUrls:['delete.component.css']
})
export class DeleteComponent implements OnInit{
    questions:Question[]=[]
    constructor(
        @Inject(DOCUMENT) private _document:Document,
        private httpservice:HttpConnectionService, 
        private router:Router,
        private storage:StorageService){  }

    ngOnInit(){
        this.httpservice.getQuestions().subscribe(response=>this.questions=response)
    }
    Delete(id:number, index:number){
        let result = confirm(`Вы действительно хотите удалить вопрос ${index + 1} ?`)
         if(result){
             this.httpservice.DeleteQuestion(id).subscribe(response=>{
                console.log('response from delete ',response)
             }, error=>{
                console.log('error from delete ', error)
                if(error.status == 403){
                    this.router.navigate(['noauth'])
                }
             })
            }else{
                return
            }
        this.router.navigate(['succed'])
    }

    Edit(id:number){
        console.log('edit questions with id',id)
        this.storage.SetValue(id)
        this.router.navigate(['editquestion'])
    }
}