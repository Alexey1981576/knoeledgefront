import { Component, OnInit } from "@angular/core";
import { HttpConnectionService } from "src/app/services/auth.service";
import { AuthUser } from "src/app/Interfaces/interfaces";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector:'login-admin',
    templateUrl:'login.component.html',
    styleUrls:['login.component.css']
})

export class LoginComponent implements OnInit{
    authuser:AuthUser
    form:FormGroup
    IsAuthentificated:boolean = false
    constructor(
        private httpservice:HttpConnectionService, 
        private router: Router){}
    HideAdmin(){
        this.IsAuthentificated = true
    }
    ngOnInit(){
        this.form = new FormGroup({
            username:new FormControl(null,[
                Validators.required,
                Validators.email
            ]),
            password:new FormControl(null,[
                Validators.required,
                Validators.minLength(6)
            ]) 
        })
    }
    Submit(){
        console.log('submit is working')
        if(this.form.invalid)
        {
            return
        }
        
        this.httpservice.login(this.form.value).subscribe(response=>{
            this.authuser=response
            sessionStorage.setItem("tokenKey",this.authuser.acces_token);
            console.log('Form value is',this.form.value)
            this.form.reset()
            this.router.navigate(['/complete'])
            
        }, error => {
            if(error.status == 400){
                console.log('have to be routing')
                this.router.navigate(['/noauth'])
            }
        })
    }

}