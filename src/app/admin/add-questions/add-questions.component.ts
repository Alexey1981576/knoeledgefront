import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { Question } from "src/app/Interfaces/interfaces";
import { HttpConnectionService } from "src/app/services/auth.service";


@Component({
    selector:'add-questions',
    templateUrl:'add-question.component.html',
    styleUrls:['add-question.component.css']
})
export class AddQuestionComponent implements OnInit{
    form:FormGroup
    constructor(private httpservice:HttpConnectionService, private router:Router){}
    ngOnInit(){
        this.form = new FormGroup({
            theQuestion:new FormControl('',[Validators.required,Validators.minLength(6)]),
            answer_1:new FormControl('', Validators.required),
            answer_2:new FormControl('',Validators.required),
            answer_3:new FormControl('', Validators.required),
            answer_4:new FormControl('', Validators.required),
            rightAnswer:new FormControl('',Validators.required)
        })
    }
    SubmitForm(question:Question){
         this.httpservice.PostQuestion(question).subscribe(response=>{
             console.log(response)
             this.form.reset()
            this.router.navigate(['succed'])
         })
        
    }
}