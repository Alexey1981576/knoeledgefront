import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddQuestionComponent } from './admin/add-questions/add-questions.component';
import { AppBodyComponent } from './questions/questions.component';
import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error-page/error-page.component';
import { LoginComponent } from './admin/login/login.component';
import { DeleteComponent } from './admin/delete-question/delete.component';
import { EditComponent } from './edit/edit.component';
import { CompleteComponent } from './complete/complete.component';
import { AuthGuard } from './services/auth.guard';
import { NoAuth } from './shared/noauth/no-auth.component';
import { SuccedComponent } from './shared/succed/succed.component';

//http://localhost:4200/ ->HomeComponent
//http://localhost:4200/test -> AppBodyComponent
//http://localhost:4200/admin -> LoginComponent
//http://localhost:4200/questions -> AddQuestionsComponent
//http://localhost:4200/editquestions -> DeleteComponent
//http://localhost:4200/editquestion -> EditComponent
//http://localhost:4200/complete ->Redirect after CRUD operation

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'questions',component:AddQuestionComponent,canActivate:[AuthGuard]},
  {path:'editquestions', component:DeleteComponent, canActivate:[AuthGuard]},
  {path:'editquestion',component:EditComponent, canActivate:[AuthGuard]},
  {path:'test', component:AppBodyComponent},
  {path:'complete', component:CompleteComponent},
  {path:'succed',component:SuccedComponent},
  {path:'noauth',component:NoAuth},
  {path:'error',component:ErrorComponent},
  {path:'**', redirectTo:'/error'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
