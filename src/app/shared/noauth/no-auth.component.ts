import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector:'no-auth',
    templateUrl:'no-auth.component.html',
    styleUrls:['no-auth.component.css']
})
export class NoAuth{
    constructor(private router:Router){}

    ToEdit(){
        this.router.navigate(['/editquestions'])
    }
    ToAdd(){
        this.router.navigate(['/questions'])
    }
}