import { Route } from "@angular/compiler/src/core";
import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector:'succed-comp',
    templateUrl:'succed.component.html',
    styleUrls:['succed.component.css']
})
export class SuccedComponent{
    constructor(private router:Router){}
    ToAdd(){
        this.router.navigate(['questions'])
    }
    ToEdit(){
        this.router.navigate(['editquestions'])
    }
}