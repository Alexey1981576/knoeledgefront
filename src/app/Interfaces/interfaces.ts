
export interface Question{
    id:number
    theQuestion:string
    answer_1:string
    answer_2:string
    answer_3:string
    answer_4:string
    rightAnswer:string
}

export interface AuthUser {
    acces_token:string
    username:string
}