import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpConnectionService } from '../services/auth.service';
import { StorageService } from '../services/storage.service';
import { Question } from '../Interfaces/interfaces';
import { Router } from '@angular/router';
import { error } from 'protractor';
@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit{
  question:Question
  form:FormGroup
  constructor(private httpservice:HttpConnectionService,
    private storage:StorageService,
    private router:Router){}
  ngOnInit(){
      this.form=new FormGroup({
        theQuestion:new FormControl('',),
        answer_1:new FormControl(''),
        answer_2:new FormControl(''),
        answer_3:new FormControl(''),
        answer_4:new FormControl(''),
        rightAnswer:new FormControl('')
      })
      this.httpservice.GetQuestionById(this.storage.GetStorage()).subscribe(response=>{
          this.question=response
         //console.log('from edit',response)
      })
  }
  Submit(){
    let values = this.form.value
    for(let row in values){
      if(values[row]=='')
      values[row]=this.question[row]
    }
    values['id']=this.question['id']

    this.httpservice.PutQuestion(this.question.id, values).subscribe(response=>{
      console.log('response is', response)
    }, error =>{
      console.log('error is', error)
    })
    this.form.reset()
    this.router.navigate(['succed'])
  }
}
