import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthUser, Question } from "../Interfaces/interfaces";

@Injectable()

export class HttpConnectionService{

    questions:Question[]=[]
    constructor(private httpclient:HttpClient){}

    getQuestions(): Observable<Question[]>{
        return this.httpclient.get<Question[]>('https://localhost:44321/Questions/GetQuestions',{
            headers: new HttpHeaders()
            .set("Accept","Application/json")
            .set("Authorization","Bearer "+sessionStorage.getItem("tokenKey"))
            
        });
        
    }

    GetQuestionById(id:number){
        return this.httpclient.get<Question>(`https://localhost:44321/Questions/${id}`)
    }

    PostQuestion(question:Question): Observable<any>{
        return this.httpclient.post('https://localhost:44321/Questions', question, {
            headers:new HttpHeaders()
            .set("Accept","application/json")
            .set("Authorization","Bearer "+sessionStorage.getItem('tokenKey'))
        })
    }

    DeleteQuestion(id:number):Observable<any>{
       return this.httpclient.delete(`https://localhost:44321/Questions/${id}`,{
           headers:new HttpHeaders()
           .set("Accept","application/json")
           .set("Authorization","Bearer "+sessionStorage.getItem('tokenKey'))
       })
    }

    PutQuestion(id:number, question:Question){
        return this.httpclient.put(`https://localhost:44321/Questions/${id}`, question, {
            headers:new HttpHeaders()
            .set("Accept","application/json")
            .set("Authorization","Bearer "+sessionStorage.getItem('tokenKey'))
        })
    }

    login(form): Observable<AuthUser> | any{
        return this.httpclient.post<AuthUser>('https://localhost:44321/token', form)
    }

    get token(): string{
        return sessionStorage.getItem('tokenKey')
    }

    IsAuthenticated():Promise<boolean> {
        return new Promise(resolve=>{
            resolve(!!this.token)
        })
    }
}