import { Injectable } from "@angular/core";

@Injectable()
export class StorageService{
    public storage:any=[]

    GetStorage(){
        return this.storage
    }

    SetValue(value:any){
        this.storage=value
    }
}