import { Component, OnInit } from "@angular/core";
import { Question } from "../Interfaces/interfaces";
import { HttpConnectionService } from "../services/auth.service";

@Component({
    selector:'app-body',
    templateUrl:'questions.component.html',
    styleUrls:['questions.component.css']
})



export class AppBodyComponent implements OnInit{

    lastanswer:string
    IsNotEnd:boolean=true
    rightanswers:number=0
    questions:Question[]=[]
    item:Question
    index:number=0
    constructor(private httpservice: HttpConnectionService){
    }

    ngOnInit(){
        this.httpservice.getQuestions()
        .subscribe(response=>{
            this.questions = response
            console.log('questions is ', this.questions)
        })
        
    }

    nextQuestion(event){
        //this.IsNotEnd=false
        if(event.target.innerText === this.questions[this.index].rightAnswer)
        {
            this.rightanswers++
        }

        if(this.index < this.questions.length)
        {
            this.index++
        }
        if(this.index == this.questions.length)
        {
            this.IsNotEnd=false
            if(this.rightanswers == this.questions.length)
            {
                this.lastanswer='supersmart'
            }
            else{
                this.lastanswer='clever'
            }
        }
        
    }

}