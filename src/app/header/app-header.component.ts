import { Component, OnInit } from "@angular/core";
import { HttpConnectionService } from "../services/auth.service";

@Component({
    selector:'app-header',
    templateUrl:'app-header.component.html',
    styleUrls:['app-header.component.css']
})
export class AppHeaderComponent implements OnInit{
    isAuthenticated:boolean
    constructor(private auth:HttpConnectionService){}
    ngOnInit(): void {
        this.isAuthenticated = !!this.auth.token
            
    }
    Logout(){
        sessionStorage.clear()
        this.isAuthenticated = false
    }
}