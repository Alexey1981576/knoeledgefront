import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppBodyComponent } from './questions/questions.component';
import { AppHeaderComponent } from './header/app-header.component';

import { ErrorComponent } from './error-page/error-page.component';
import { HttpConnectionService } from './services/auth.service';
import { LoginComponent } from './admin/login/login.component';
import { AddQuestionComponent } from './admin/add-questions/add-questions.component';
import { DeleteComponent } from './admin/delete-question/delete.component';

import { StorageService } from './services/storage.service';
import { EditComponent } from './edit/edit.component';
import { CompleteComponent } from './complete/complete.component';
import { NoAuth } from './shared/noauth/no-auth.component';

@NgModule({
  declarations: [
    AppComponent,
    AppHeaderComponent,
    AppBodyComponent,
    LoginComponent,
    AddQuestionComponent,
    DeleteComponent,
    ErrorComponent,
    EditComponent,
    CompleteComponent,
    NoAuth
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [HttpConnectionService, StorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
